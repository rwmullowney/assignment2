from flask import Flask, render_template, request, redirect, url_for, jsonify, Response
import random
from helpers import id_to_index, find_available_book_id
app = Flask(__name__)

books = [{'title': 'Software Engineering', 'id': '1'},
         {'title': 'Algorithm Design', 'id': '2'},
         {'title': 'Python', 'id': '3'}]


@app.route('/book/JSON')
def bookJSON():
    return jsonify(books)


@app.route('/')
@app.route('/book/')
def showBook():
    return render_template('showBook.html', books=books)


@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
    if request.method == "POST":
        if request.form['new_book']:
            available_id = find_available_book_id(books)
            books.append({
                'title': request.form['new_book'],
                'id': available_id
            })
        return redirect(url_for('showBook'))

    return render_template('newBook.html')


@app.route('/book/<int:book_id>/edit/', methods=['GET', 'POST'])
def editBook(book_id):
    book_idx = id_to_index(books, book_id)
    if request.method == "POST":
        books[book_idx]['title'] = request.form['book_name']
        return redirect(url_for('showBook'))

    return render_template('editBook.html', book=books[book_idx])


@app.route('/book/<int:book_id>/delete/', methods=['GET', 'POST'])
def deleteBook(book_id):
    book_idx = id_to_index(books, book_id)
    if request.method == "POST":

        del books[book_idx]

        return redirect(url_for('showBook'))

    return render_template('deleteBook.html', book=books[book_idx])


if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0', port=5001)
