def id_to_index(books, book_id):
    ''' Find the index for our given book ID '''
    book_id = str(book_id)
    book_ids = list_book_ids(books)
    return book_ids.index(book_id)


def find_available_book_id(books):
    ''' Determine the first available book ID '''
    book_ids = list_book_ids(books)
    new_book_id = 1
    while True:
        if str(new_book_id) in book_ids:
            new_book_id += 1
        else:
            return str(new_book_id)


def list_book_ids(books):
    ''' Return a list of book IDs '''
    book_ids = []
    for book in books:
        book_ids.append(book['id'])
    return book_ids